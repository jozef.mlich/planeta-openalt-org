CONTAINER_IMAGE=registry.gitlab.com/openalt/planet-venus-container:latest
THEME=$(shell grep ^output_theme config.ini | cut -d '=' -f 2 | tr -d ' ')
THEME_DIR=themes/$(THEME)
LANG=cs_CZ.UTF-8

build:
	cp -r ${PWD}/$(THEME_DIR) ${VENUS_DIR}/$(THEME_DIR)
	LANG=$(LANG) planet.py ./config.ini

build_in_docker:
	docker pull $(CONTAINER_IMAGE)
	docker run \
		--workdir $(PWD) \
		-v $(PWD):$(PWD) \
		--rm=true \
		--entrypoint=/bin/sh \
		$(CONTAINER_IMAGE) -c make build

build_in_podman:
	podman pull $(CONTAINER_IMAGE)
	podman run \
		--workdir $(PWD) \
		-v $(PWD):$(PWD):Z \
		--rm=true \
		--entrypoint=/bin/sh \
		$(CONTAINER_IMAGE) -c make build

clean:
	find cache -mindepth 1 -maxdepth 1 -not -name '.keep' -exec rm -rf '{}' \;
	find html -mindepth 1 -maxdepth 1 -not -name '.keep' -exec rm -rf '{}' \;
	find themes -name '*.tmplc' -exec rm -rf '{}' \;
