# Planeta.OpenAlt.org

Repositář pro agregátor Planeta.OpenAlt.org. Používá software Planet Venus ([v kontejneru](https://gitlab.com/OpenAlt/planet-venus-container/)) a výsledný statický obsah je hostován pomocí [GitLab Pages](https://docs.gitlab.com/ce/user/project/pages/). Sestavení probíhá pravidelně několikrát denně s využitím [GitLab CI/CD](https://docs.gitlab.com/ce/ci/).

## Úpravy obsahu

### Šablona
Soubory tvořící základní HTML šablonu se nachází v adresáři `themes/openalt`. Jako základ využívá `themes/common` přímo z Planet Venus. Pokud pracujete na úpravě šablony, před sestavením (viz níže) je vždy potřeba vyčistit zkompilované soubory šablony `.tmplc`, např. pomocí tohoto příkazu.
```
$ make clean
```

### Obsah
Obsah stránky se generuje z RSS feedů uvedených v souboru `config.ini`, který obsahuje veškerou konfiguraci Planet Venus.

### Příprava
Abyste byli schopni spustit níže uvedené příkazy, je nutné mít nainstalovaný _make_ a [Docker](https://docs.docker.com/get-docker/) nebo [Podman](https://podman.io/getting-started/installation).

## Sestavení statické verze
Pokud používáte Docker, pro sestavení můžete použít tento příkaz.
```
$ make build_in_docker
```
Na systémech, kde Docker nefunguje, nebo pokud upřednostňujete alternativní Podman, použijte tento příkaz.
```
$ make build_in_podman
```
Statická verze je vygenerovaná do adresáře `html`. Pro nasazení stačí jeho obsah nahrát na server třeba přes FTP.
